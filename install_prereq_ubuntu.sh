#!/bin/bash

apt-get install -y \
  build-essential \
  curl \
  git \
  freeglut3-dev \
  libboost-dev \
  libboost-filesystem-dev \
  libboost-program-options-dev \
  libboost-test-dev \
  libeigen3-dev \
  libexpat-dev \
  libftgl-dev \
  libgl2ps-dev \
  libglew-dev \
  libgsl-dev \
  liblz4-dev \
  liblzma-dev \
  libpcre3-dev \
  libtbb-dev \
  libx11-dev \
  libxext-dev \
  libxft-dev \
  libxpm-dev \
  libxerces-c-dev \
  libxxhash-dev \
  libzstd-dev \
  ninja-build \
  python3 \
  python3-dev \
  python3-pip \
  rsync \
  zlib1g-dev
